//
//  HorizontalViewController.swift
//  Leleknaptar
//
//  Created by Othmen on 2018. 01. 22..
//  Copyright © 2018. MekomKft. All rights reserved.
//

import UIKit

class HorizontalViewController: UIViewController,UIScrollViewDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    
    var currentWeek:Int = 0
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var viewControllers = [UIViewController]()
    var view1:UIView!

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        var easter = Date.easterHoliday(year:2022)
        if easter!.compare(Date.init()) == ComparisonResult.orderedDescending {
            easter = Date.easterHoliday(year:2021)
        }
        
        self.scrollView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height)
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width - 30
        let height = bounds.size.height;
        let date = Date()
        let calendar = NSCalendar.current
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: date)
        let date2 = calendar.startOfDay(for: easter!)
        let components = calendar.dateComponents([.day], from: date2, to: date1)
        currentWeek = components.day! / 7 + 1
        scrollView.contentOffset = CGPoint(x:CGFloat(currentWeek-1)*width,y:0)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        for i in 0..<appDelegate.soulcalendars.count {
            let vc = storyboard.instantiateViewController(withIdentifier: "viewController") as! ViewController;
            vc.index=i
            vc.soulcalendar = appDelegate.soulcalendars[i] as AnyObject
            viewControllers.append(vc)
        }
        
        scrollView!.contentSize = CGSize(width: CGFloat(appDelegate.soulcalendars.count)*width,height: height);
        

        var idx:Int = 0;
        for viewController in viewControllers {
            addChildViewController(viewController);
            let originX:CGFloat = CGFloat(idx) * width;
            view1 = UIView()
            view1.frame = CGRect(x:originX ,y: 0,width: width, height:height)
            view1!.addSubview(viewController.view)
            scrollView.addSubview(view1)
            self.scrollView.bringSubview(toFront: view1)
            viewController.didMove(toParentViewController: self)
            idx=idx+1;
        }
      
        self.scrollView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifierScrollHorizontal"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification){
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width - 30
        scrollView.contentOffset = CGPoint(x:CGFloat(currentWeek-1)*width, y:0.0)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width - 30
        let langIndex = Int((scrollView.bounds.origin.x/width))
        let language = Locale.preferredLanguages[0]
        var textGoToCurrent : String = ""
        var textCurrent : String = ""
        if (language.range(of: "hu")) != nil {
            textGoToCurrent="Ugrás az aktuális hétre"
            textCurrent="Aktuális hét"
        } else {
            textGoToCurrent="Go to current week"
            textCurrent="Current week"
        }
        if(langIndex != currentWeek-1){
            let textGoToCurrent:[String: String] = ["textLabel": textGoToCurrent]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifierChangeText"), object: nil, userInfo: textGoToCurrent)
        }else{
            let textCurrent:[String: String] = ["textLabel": textCurrent]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifierChangeText"), object: nil, userInfo: textCurrent)
        }
    }
}


extension Date {

    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {

        let currentCalendar = Calendar.current

        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }

        return end - start
    }
    
    static func easterHoliday(year: Int) -> Date? {
        guard let dateComponents = Date.dateComponentsForEaster(year: year) else { return nil }
        return Calendar.current.date(from: dateComponents)
    }

    static func dateComponentsForEaster(year: Int) -> DateComponents? {
        // Easter calculation from Anonymous Gregorian algorithm
        // AKA Meeus/Jones/Butcher algorithm
        let a = year % 19
        let b = Int(floor(Double(year) / 100))
        let c = year % 100
        let d = Int(floor(Double(b) / 4))
        let e = b % 4
        let f = Int(floor(Double(b+8) / 25))
        let g = Int(floor(Double(b-f+1) / 3))
        let h = (19*a + b - d - g + 15) % 30
        let i = Int(floor(Double(c) / 4))
        let k = c % 4
        let L = (32 + 2*e + 2*i - h - k) % 7
        let m = Int(floor(Double(a + 11*h + 22*L) / 451))
        var dateComponents = DateComponents()
        dateComponents.month = Int(floor(Double(h + L - 7*m + 114) / 31))
        dateComponents.day = ((h + L - 7*m + 114) % 31) + 1
        dateComponents.year = year
        guard let easter = Calendar.current.date(from: dateComponents) else { return nil } // Convert to calculate weekday, weekdayOrdinal
        return Calendar.current.dateComponents([.year, .month, .day, .weekday, .weekdayOrdinal], from: easter)
    }

}

