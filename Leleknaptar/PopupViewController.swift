//
//  PopupViewController.swift
//  Leleknaptar
//
//  Created by Othmen on 2018. 01. 26..
//  Copyright © 2018. MekomKft. All rights reserved.
//

import UIKit

class PopupViewController: UIViewController {
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var textLabel: UILabel!
    var text:String = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        popupView.layer.cornerRadius = 8;
        popupView.layer.masksToBounds = true;
        //get phone language and set the translation of texts
        let language = Locale.preferredLanguages[0]
        
        if (language.range(of: "hu")) != nil {
            text = "Az év lefolyásának saját élete van. S ezzel az élettel kapcsolatban az emberi lélek kibontakoztathatja együtt-érzését. Ha a lélek megnyitja magát azoknak a hatásoknak, melyek hétről-hétre oly különbözőképp szólnak hozzá, rálel önmaga helyes érzékelésére. Ezáltal olyan növekvő erőket érez majd magában, melyek megerősítik őt. Észreveszi majd, hogy az effajta belső erők azt akarják, hogy felébresszék őket – felébredjenek a képesség által, mellyel a lélek felveszi a világ történéseinek céljait, ahogyan azok az idő ritmusában az életbe belépnek. Ezáltal válik teljesen tudatossá a finom, mégis életerős szálra, mely közte és a világ között - amelybe beleszületett – ott van.\n\nEbben a Kalendáriumban minden héthez egy vers van rendelve. Ez lehetővé teszi a léleknek, hogy aktívan részt vegyen az év hétről-hétre kibontakozó, fejlődő életében. Ahogy a lélek az év életével folyamatosan összekapcsolódik, úgy kellene a verseknek visszhangoznia benne.  A szándék az, hogy a Természet életének folyamatával való ’egy-ség’ egészséges érzése, és ebből egy erőteljes ’önmagunk megtalálása’ érzés alakuljon ki, remélve azt, hogy a lélek számára egy, a világ folyásával összekötődő, érzés-harmónia alakul ki, ahogy az ezekben a versekben feltárul, s ez olyasvalami lesz, amire a lélek vágyakozni fog, amennyiben helyesen érti meg önmagát."
          }else{
            text = "The course of the year has its own life which the human soul can accompany and take part in. It may rightly find itself when such a sharing is sensitive to that which changes from week to week, and may feel how it gains strengthening forces from within. One will realize that such soul forces are waiting to be roused by participating in the meaning of the world's journeying and how it unfolds in the course of time. Thus one becomes aware of the delicate but vital threads that unite the soul with the world into which it is born.\n\nIn this calendar there is a verse for each week composed so that the soul can experience the apropriate part of the year's journey, and something of the life that can thrive in the soul when it is thus united. Then a healthy feeling of at-one-ness with the course of nature and a strong sense of self can develop, for undoubtedly, the soul bears a yearning to participate in the world's journeying by means of what these verses offer."
        }
        textLabel.text = text
    }

    @IBAction func hidePopupAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
