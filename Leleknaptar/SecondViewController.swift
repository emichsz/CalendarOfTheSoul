//
//  SecondViewController.swiftRadiusView
//  Leleknaptar
//
//  Created by Othmen on 2018. 01. 22..
//  Copyright © 2018. MekomKft. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var weekLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var writerName: UILabel!
    @IBOutlet weak var translationLabel: UILabel!
    @IBOutlet weak var mView: UIView!
    @IBOutlet weak var bottomWeekLabel: UILabel!
    
    @IBOutlet weak var moveToCurrent: UIButton!
    var content : AnyObject?
    var index : Int = 0
    var indexWriter : Int = 0
    var transltaionText:String?
    var currentweekText:String?
    var cancelText:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mView.layer.cornerRadius = 8;
        mView.layer.masksToBounds = true;
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height;
        //get phone language and set the translation of texts
        let language = Locale.preferredLanguages[0]
        setTransltaion(lang: language)
        translationLabel.text=transltaionText
        weekLabel.text = "\(index+1)"
        bottomWeekLabel.text=currentweekText
        if let languageText:  String = appDelegate.versions[indexWriter].value(forKeyPath: "desc") as?String{
            writerName.text=languageText
        }
        if let text:  String = content?.value(forKeyPath: "text") as?String{
            let stringValue = text
            let attrString = NSMutableAttributedString(string: stringValue)
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 15 // change line spacing between paragraph
            style.minimumLineHeight = height==480 ? 15 : 18 // change line spacing between each line
            attrString.addAttribute(NSAttributedStringKey.paragraphStyle, value: style, range: NSRange(location: 0, length: (stringValue.count)))
            textLabel.attributedText=attrString
        }
        if(height==480||height==568){
            textLabel.font = UIFont(name: "Cochin", size: 13)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifierChangeText"), object: nil)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification){
        if let textLabel = notification.userInfo?["textLabel"] as? String {
            bottomWeekLabel.text=textLabel
            if(textLabel=="Ugrás az aktuális hétre"||textLabel=="Go to current week"){
                bottomWeekLabel.textColor = .red
            }else{
                bottomWeekLabel.textColor = .white
            }
        }
    }
    
    func setTransltaion(lang:String) {
        if (lang.range(of: "hu")) != nil {
            transltaionText="Verzió:"
            currentweekText="Aktuális hét"
            cancelText="Mégse"
        }
        else{
            transltaionText="Version:"
            currentweekText="Current week"
            cancelText="Cancel"
        }
    }
    
    @IBAction func showAlertAction(_ sender: Any) {
        showPopTranslation()
    }
    
    @IBAction func moveToCurrent(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifierScrollHorizontal"), object: nil, userInfo: nil)
        let language = Locale.preferredLanguages[0]
        var textCurrent : String = ""
        
       if (language.range(of: "hu")) != nil{
            textCurrent = "Aktuális hét"
        }else{
            textCurrent = "Current week"
        }
        
        let currentweekText:[String: String] = ["textLabel": textCurrent]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifierChangeText"), object: nil, userInfo: currentweekText)
    }
    
    func showPopTranslation(){
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for i in 0..<appDelegate.versions.count {
            let title = appDelegate.versions[i].value(forKeyPath: "desc") as? String
            let action = UIAlertAction(title: title, style: .default) { action in
                let imageDataDict:[String: Int] = ["langIndex": i]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil, userInfo: imageDataDict)
            }
            alertController.addAction(action)
        }
        let cancelAction = UIAlertAction(title: cancelText, style: .destructive) { action in}
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true)
    }
    
    @IBAction func showTextPopup(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let popup = storyboard.instantiateViewController(withIdentifier: "popupViewController") as! PopupViewController;
        navigationController?.present(popup, animated: true)
    }
}
