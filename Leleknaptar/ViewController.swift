//
//  ViewController.swift
//  Leleknaptar
//
//  Created by Othmen on 2017. 11. 17..
//  Copyright © 2017. MekomKft. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIScrollViewDelegate {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    var viewControllers = [UIViewController]()
    var soulcalendar :AnyObject?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var index : Int = 0
    @IBOutlet weak var scrollViewContentHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        self.scrollView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height)
    
        super.viewDidLoad()
        scrollView.delegate = self
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        //we will get the nbr of languages from the json
        if let contents: Array<AnyObject> = soulcalendar?.value(forKeyPath: "content") as? Array<AnyObject>{
            for i in 0..<contents.count {
                let vc = storyboard.instantiateViewController(withIdentifier: "secondViewController") as! SecondViewController;
                vc.content = contents[i]
                vc.index = index
                vc.indexWriter = i
                viewControllers.append(vc)
            }
            
            let constraint = NSLayoutConstraint(item: scrollViewContentHeightConstraint.firstItem, attribute: scrollViewContentHeightConstraint.firstAttribute, relatedBy: scrollViewContentHeightConstraint.relation, toItem: scrollViewContentHeightConstraint.secondItem, attribute: scrollViewContentHeightConstraint.secondAttribute, multiplier: CGFloat(contents.count), constant: scrollViewContentHeightConstraint.constant)
            view.removeConstraint(scrollViewContentHeightConstraint)
            scrollViewContentHeightConstraint = constraint
            view.addConstraint(constraint)
            view.layoutIfNeeded()
            
            let bounds = UIScreen.main.bounds
            let width = bounds.size.width - 30
            let height = bounds.size.height;
            scrollView!.contentSize = CGSize(width: width, height: CGFloat(contents.count) * height);

            let defaults = UserDefaults.standard
            let languageSelectedIndex = defaults.integer(forKey: "languageSelectedIndex")
            scrollView.contentOffset = CGPoint(x: 0.0, y:(CGFloat(languageSelectedIndex) * height))

            var idy:Int = 0;
            for viewController in viewControllers {
                addChildViewController(viewController)
                let originY:CGFloat = CGFloat(idy) * height
                viewController.view.frame = CGRect(x:0,y: originY, width: width, height:height)
                scrollView!.addSubview(viewController.view)
                viewController.didMove(toParentViewController: self)
                idy=idy+1;
            }
        }

        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }


    @objc func methodOfReceivedNotification(notification: Notification){
        
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height;
        if let langIndex = notification.userInfo?["langIndex"] as? Int {
              scrollView.contentOffset = CGPoint(x: 0.0, y:(CGFloat(langIndex) * height))
        }
    }
  
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height;
        let langIndex = Int(scrollView.bounds.origin.y/height)
        let imageDataDict:[String: Int] = ["langIndex": langIndex]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil, userInfo: imageDataDict)
        let defaults = UserDefaults.standard
        defaults.set(langIndex, forKey: "languageSelectedIndex")
    }

}










